package utils;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.testng.ITestResult;
import org.testng.internal.IResultListener;

public class TestNgListener implements IResultListener {
	
	public void onTestStart(ITestResult result) {
		Log.info("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
		Log.info("++++ Start test case : " + result.getMethod().getMethodName() + "++++");
		Log.info("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
		String timestamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());
		Log.info("Start time : " + timestamp);
	}
	
	public void onTestSuccess(ITestResult result) {
		Log.info("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
		Log.info("++++ End test case : " + result.getMethod().getMethodName() + "++++");
		Log.info("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
		String timestamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());
		Log.info("End time : " + timestamp);
	}
	
	public void onTestFailure(ITestResult result) {
		Log.error("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
		Log.error("++++ Failed test case : " + result.getMethod().getMethodName() + "++++");
		Log.error("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
		String timestamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());
		Log.error("End time : " + timestamp);
		Log.error(result.getThrowable());
	}
	
	public void onTestSkip(ITestResult result) {
		System.out.println("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
		System.out.println("++++ Skiped test case : " + result.getMethod().getMethodName() + "++++");
		System.out.println("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
	}
	
}
