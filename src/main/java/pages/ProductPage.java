package pages;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ProductPage {

public WebDriver driver;
	
	public ProductPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(className ="woocommerce-product-gallery__trigger") 
	public WebElement buttonMagnifier;
	
	@FindBy(xpath="//div[contains(@class,'star-rating') and ancestor::div[contains(@class, 'summary')]]") 
	public WebElement productRating;
	
	@FindBy(xpath="//bdi[ancestor::div[contains(@class, 'summary')]]") 
	public WebElement labelPrice;
	
	@FindBy(xpath="//input[contains(@class, 'qty') and ancestor::div[contains(@class, 'summary')]]") 
	public WebElement inputQuantity;
	
	@FindBy(name="add-to-cart") 
	public WebElement buttonAddToCart;
	
	@FindBy(xpath="//a[@rel='tag' and parent::span[contains(@class,'posted_in')]]") 
	public List<WebElement> tagsCategories;

}
