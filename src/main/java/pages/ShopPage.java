package pages;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import utils.SeleniumWrappers;

public class ShopPage extends SeleniumWrappers{

	public WebDriver driver;
	
	public ShopPage(WebDriver driver) {
		super(driver);
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(name="orderby") 
	public WebElement orderDropDown;
	
	@FindBy(xpath="//bdi[ancestor::span[contains(@class,'price')] and not(ancestor::*[contains(@aria-hidden, 'true')])]")
	public List<WebElement> listedProductsPrices;
	
	@FindBy(xpath="//li[contains(@class, 'product')]")
	public List<WebElement> listedProducts;
	
	@FindBy(css="span[style='left: 0%']")
	public WebElement sliderInitialPosition;
	
	@FindBy(css="span[style='left: 100%']")
	public WebElement sliderFinalPosition;
	
	@FindBy(css="a[class*='add_to_cart_button']")
	public WebElement addToCart;
	
	public void filterByValue(String value) {
		Select selectDropdown = new Select(orderDropDown);
		selectDropdown.selectByValue(value);
	}
}
