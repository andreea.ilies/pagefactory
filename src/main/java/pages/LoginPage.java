package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import utils.ReadProperties;
import utils.SeleniumWrappers;

public class LoginPage extends SeleniumWrappers {

	public WebDriver driver;
	public String username = ReadProperties.credentials.getProperty("user");
	public String password = ReadProperties.credentials.getProperty("pass");
	
	public LoginPage(WebDriver driver) {
		super(driver);
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(id="log") 
	public WebElement usernameField;
	
	@FindBy(id="password") 
	public WebElement passwordField;
	
	@FindBy(css="input[class='submit_button']") 
	public WebElement submitButton;
	
	@FindBy(css = "li[class='menu_user_logout']")
	public WebElement logoutButton;
	
	@FindBy(css = "a[class*='popup_login_link']")
	public WebElement loginLink;
	
	public void loginInApp() {
		//usernameField.sendKeys(username);
		//passwordField.sendKeys(password);
		sendKeys(usernameField, username);
		sendKeys(passwordField, username);
		
		//submitButton.click();
		click(submitButton);
	}
	
	public void logoutFromApp() {
		//logoutButton.click();
		click(logoutButton);
	}
}
