package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import utils.SeleniumWrappers;

public class AudioPostPage extends SeleniumWrappers{

	public AudioPostPage(WebDriver driver) {
		super(driver);
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(css = "div[class*='playpause-button']")
	public WebElement playButton;
	
	@FindBy(css = "span[class*='time-current']")
	public WebElement timeSlider;
	
	@FindBy(css = "div[class*='volume-current']")
	public WebElement volumeSlider;
}
