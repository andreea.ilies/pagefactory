package tests;

import java.util.List;
import java.util.ArrayList;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import utils.BaseTest;
import utils.TestNgListener;

@Listeners(TestNgListener.class)
public class IframeExample extends BaseTest {

	@Test
	public void iFrameExample() throws InterruptedException {
		page.navMenu.navigateTo(page.navMenu.contactLink);
		page.contactPage.zoomMap(page.contactPage.zoomOut);
		Thread.sleep(5000);
	}
	
	@Test
	public void iframeExample2() throws InterruptedException {
		page.navMenu.navigateTo(page.navMenu.eventstLink);
		page.eventPage.click(page.eventPage.festivalOfOldFilms);
		page.singleEventPage.clickMap();
		Thread.sleep(5000);
		
		List<String> browserTabs = new ArrayList<>(driver.getWindowHandles());
		
		driver.switchTo().window(browserTabs.get(1));
		driver.close();
		Thread.sleep(5000);
	}
}
