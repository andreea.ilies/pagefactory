package tests;

import org.testng.annotations.Test;

import pages.LoginPage;
import pages.NavMenuPage;
import utils.BaseTest;

public class LoginTests extends BaseTest{

	NavMenuPage navMenu;
	LoginPage loginPage;
	
	@Test
	public void loginTest() {
		
		navMenu = new NavMenuPage(driver);
		loginPage = new LoginPage(driver);
		
		navMenu.navigateTo(navMenu.loginLink);
		loginPage.loginInApp();
	}
	
	@Test
	public void invalidLoginTest() {
		
		navMenu = new NavMenuPage(driver);
		loginPage = new LoginPage(driver);
		
		navMenu.navigateTo(navMenu.loginLink);
		loginPage.loginInApp();
	}
}
