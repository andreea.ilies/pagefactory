package tests;

import static org.testng.Assert.assertTrue;

import org.testng.annotations.Test;
import pages.NavMenuPage;
import pages.ProductPage;
import pages.ShopPage;
import utils.BaseTest;

public class ShopTest extends BaseTest{

	NavMenuPage navMenu;
	ShopPage shopPage;
	ProductPage productPage;
	
	@Test
	public void test1() {
		
		navMenu = new NavMenuPage(driver);
		shopPage = new ShopPage(driver);
		
		navMenu.navigateTo(navMenu.shopLink);
		shopPage.filterByValue("price");
		
		float firstBookPrice = Float.parseFloat(shopPage.listedProductsPrices.get(0).getText().substring(1));
		float lastBookPrice = Float.parseFloat(shopPage.listedProductsPrices.get(shopPage.listedProductsPrices.size() - 1).getText().substring(1));
		
		assertTrue(firstBookPrice < lastBookPrice);
	}
	
	@Test (dependsOnMethods = {"test1"})
	public void test2() {
		
		productPage = new ProductPage(driver);
		
		shopPage.listedProducts.get(2).click(); //am ales a 3-a carte pt ca are raging, majoritatea nu au
		
		assertTrue(productPage.buttonMagnifier.isDisplayed());
		assertTrue(productPage.productRating.isDisplayed());
		assertTrue(productPage.labelPrice.isDisplayed());
		assertTrue(productPage.inputQuantity.isDisplayed());
		assertTrue(productPage.buttonAddToCart.isDisplayed());
		assertTrue(productPage.tagsCategories.get(0).isDisplayed());
		
	}
}
