package tests;

import org.testng.annotations.Test;
import utils.BaseTest;

public class Tema10 extends BaseTest{

	@Test
	public void AudioPostSliders() throws InterruptedException {
		page.navMenu.hoverElement(page.navMenu.blogLink);
		page.navMenu.navigateTo(page.navMenu.blogSubMenuPostFormatLink);
		Thread.sleep(2000);
		page.postFormats.hoverElement(page.postFormats.audioPost);
		page.postFormats.click(page.postFormats.audioPost);
		
		page.audioPostPage.click(page.audioPostPage.playButton);
		page.audioPostPage.dragAndDropSlider(page.audioPostPage.timeSlider, 200, 0);	
		page.audioPostPage.click(page.audioPostPage.playButton);
		Thread.sleep(2000);
		page.audioPostPage.dragAndDropSlider(page.audioPostPage.volumeSlider, 50, 0);	
		Thread.sleep(2000);
	}
}
